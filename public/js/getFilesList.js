$.ajax({
  url: "./utils/NetauraEquipesLinks.json",
  method: "GET",
  data: {},
  dataType: "json",
  success: function (result) {
    result.sort( ( a, b ) => { return a.index - b.index } );

    var select = document.getElementById("equipes");
    for( var i in result ) {
      var el = document.createElement("option");
      el.textContent = result[i].name;
      el.value = result[i].id;
      select.appendChild(el); 
    }
  }
});