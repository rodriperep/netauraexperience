/**
 * Setup the Networked-Aframe scene component based on query parameters
 */
 AFRAME.registerComponent('dynamic-room', {
  init: function () {
    var params = this.getUrlParams();
    var myNametag = player.parentEl.querySelector('.nametag');
    myNametag.setAttribute('text', 'value', params.username);

    const table = document.getElementById('table');
    const drivelink = `https://docs.google.com/spreadsheets/d/${params.equipe}/edit?usp=sharing?&rm=minimal&single=true`;
    table.setAttribute('src', drivelink);
  },

  getUrlParams: function () {
    var match;
    var pl = /\+/g;  // Regex for replacing addition symbol with a space
    var search = /([^&=]+)=?([^&]*)/g;
    var decode = function (s) { return decodeURIComponent(s.replace(pl, ' ')); };
    var query = window.location.search.substring(1);
    var urlParams = {};

    match = search.exec(query);
    while (match) {
      urlParams[decode(match[1])] = decode(match[2]);
      match = search.exec(query);
    }
    return urlParams;
  }
});